var CryptoJS = require("crypto-js");
var axios = require("axios").default;

// var baseUrl = 'http://localhost/youfid/dev/service';
var baseUrl = "https://api.youfid.fr/dev/service";

var reqBody = { login: "login", password: "p@ss" };

axios
  .post(baseUrl + "/marchandInitMarchand.php", reqBody)
  .then(function(res) {
    var data = res.data;
    console.log("Init response", data);
    var sessionKey = data.session_key;
    var clientId = data.merchant_id;
    console.log("Key Hex: ", sessionKey);

    var keyBytes = hexStringToByte(sessionKey);
    var key = new TextDecoder("utf-8").decode(keyBytes);
    console.log("Key: ", key);

    var nonce = "123234";
    var timestamp = Math.floor(Date.now() / 1000);

    reqBody = { merchant_id: 889057 };
    /*reqBody = {
      "merchant_id": 76,
      "qr_code": "83788381",
      "answer": {
        "survey": 1,
        "note": 1,
        "marchandId": 75,
        "comment": "test"
      }
    };
    
   reqBody = {
     merchant_id: 76,
     qr_code: 83788381,
     is_register: 0,
     montant: 15,
     optout: 0
   };*/

    /*reqBody = {
    merchant_id: 76,
    usr_input: 5657,
    products: [
      {
        id: 1,
        cost: 10,
        name: 'test'
      }
    ]
   };*
  
   reqBody = {
    merchant_id: 889057,
    usr_input: 'test'
  };*/

    var dataToSign = JSON.stringify(reqBody) + clientId + nonce + timestamp;
    console.log("Data to sign: ", dataToSign);
    var signature = CryptoJS.HmacSHA256(dataToSign, key).toString(CryptoJS.enc.Hex);

    var authHeader = `fid_client_id=${clientId},nonce=${nonce},timestamp=${timestamp},sign_algo=HMAC_SHA256,sign=${signature}`;
    console.log("Auth header: ", authHeader);
    reqHeader = {
      headers: {
        authorization: authHeader
      }
    };
    return axios.post(`${baseUrl}/marchandGetMerchantDetailsV2.php`, reqBody, reqHeader);
    // return axios.post(`${baseUrl}/testmarchandGetAnswerV2.php`, reqBody, reqHeader);
    // return axios.post(`${baseUrl}/marchandCheckScanUser.php`, reqBody, reqHeader);
    // return axios.post(`${baseUrl}/transformToPresent.php`, reqBody, reqHeader);
    // return axios.post(`${baseUrl}/marchandListUsersV2.php`, reqBody, reqHeader);
  })
  .then(response => {
    console.log("Response: ", response.data);
  })
  .catch(function(error) {
    console.error("Error: ", error);
  });

function byteToHexString(uint8arr) {
  if (!uint8arr) {
    return "";
  }

  var hexStr = "";
  for (var i = 0; i < uint8arr.length; i++) {
    var hex = (uint8arr[i] & 0xff).toString(16);
    hex = hex.length === 1 ? "0" + hex : hex;
    hexStr += hex;
  }

  return hexStr.toUpperCase();
}

function hexStringToByte(str) {
  if (!str) {
    return new Uint8Array();
  }

  var a = [];
  for (var i = 0, len = str.length; i < len; i += 2) {
    a.push(parseInt(str.substr(i, 2), 16));
  }

  return new Uint8Array(a);
}
